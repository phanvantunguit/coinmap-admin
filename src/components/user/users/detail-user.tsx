import React, { useState } from 'react';
import {
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Flex,
  Box,
  useColorModeValue,
  Heading,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Textarea,
} from '@chakra-ui/react';
import { selectedCSS } from 'constants/index';
import useTabIndex from 'components/hook/tab-index';
import ListSBot from 'components/user/users/list-history-sbot';
import ListTBot from 'components/user/users/list-history-tbot';
import ListPkg from 'components/user/users/list-history-pkg';

export default function HistoryTransaction() {
  const [tabIndex, setTabIndex] = useTabIndex();
  const [openModal, setOpenModal] = useState(false);
  const [metadataLog, setMetadataLog] = useState<{ [key: string]: any }>();

  const handleChangeTab = (i: number) => {
    setTabIndex(i);
  };

  const bg = useColorModeValue('white', 'gray.700');

  const handleClickMetaData = (metadata: { [key: string]: any }) => {
    setMetadataLog(metadata);
    setOpenModal(true);
  };

  return (
    <>
      <Box bg={bg} p="20px" borderRadius={15}>
        <Heading mb={'20px'} textAlign="center" size="xl">
          TRANSACTION HISTORY
        </Heading>
        <Flex flexDirection="column">
          <Tabs
            isLazy
            index={tabIndex}
            variant="enclosed"
            onChange={handleChangeTab}
          >
            <TabList>
              <Tab _selected={selectedCSS}>PACKAGE</Tab>
              <Tab _selected={selectedCSS}>BOT SIGNAL</Tab>
              <Tab _selected={selectedCSS}>BOT TRADING</Tab>
            </TabList>
            <TabPanels>
              <TabPanel>
                <ListPkg handleClickMetaData={handleClickMetaData} />
              </TabPanel>
              <TabPanel>
                <ListSBot handleClickMetaData={handleClickMetaData} />
              </TabPanel>
              <TabPanel>
                <ListTBot handleClickMetaData={handleClickMetaData} />
              </TabPanel>
            </TabPanels>
          </Tabs>
        </Flex>
      </Box>
      <Modal
        size="4xl"
        isCentered
        isOpen={openModal}
        onClose={() => setOpenModal(false)}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Metadata</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex align="center" mb="18px">
              <Textarea
                isReadOnly
                rows={20}
                value={
                  metadataLog ? JSON.stringify(metadataLog, undefined, 2) : ''
                }
                size="lg"
              />
            </Flex>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" onClick={() => setOpenModal(false)}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
