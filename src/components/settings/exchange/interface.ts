export interface Exchange {
  exchangeName: string;
  exchangeDesc: string;
  createdAt: string;
}

export interface CreateExchange {
  exchangeName: string;
  exchangeDesc: string;
}
