export interface General {
  generalSettingId: string;
  generalSettingName: string;
  description: string;
  ownerCreated: string;
  valLimit: number;
  createdAt: string;
  updatedAt: string;
}
