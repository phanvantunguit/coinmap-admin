import Profile from 'components/profile';
import type { NextPage } from 'next';
import React from 'react';
const ProfilePage: NextPage = () => {
  return <Profile />;
};

export default ProfilePage;
