import SignIn from 'components/sign-in';
import Layout from 'layouts/auth';
import React from 'react';

const SignInPage = () => {
  return <SignIn />;
};

SignInPage.Layout = Layout;

export default SignInPage;
