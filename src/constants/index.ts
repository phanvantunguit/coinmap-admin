export enum FEATURE_ACTIONS {
  VIEW = 'view',
  EDIT = 'edit',
  DELETE = 'delete',
  CREATE = 'create',
  EXPORT = 'export',
}

export const selectedCSS = {
  bg: 'teal.300',
  color: 'white',
  fontWeight: 'bold',
};

export const FILTER_DEFAULT = {
  page: 1,
  totalPage: 1,
  size: 20,
  total: 1,
};
